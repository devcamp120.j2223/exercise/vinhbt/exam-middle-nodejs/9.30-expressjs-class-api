//Import class
//  Khai báo thư viện express
const express = require('express');

const { userRouter } = require('./app/router/userRouter');

// Khai báo app nodeJS
const app = express();



//Sử dụng Unicode
app.use(express.urlencoded({
    urlencoded: true
}))

//Khai báo cổng nodeJS
const port = 8000;



// Sử dụng router
app.use('/', userRouter);

//Chạy cổng nodeJS
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})
