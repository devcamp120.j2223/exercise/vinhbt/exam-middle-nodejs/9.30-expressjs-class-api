//  Khai báo class User
class User {
    constructor(Id, Name, Position, Office, Age, StartDate) {
        this.Id = Id;
        this.name = Name;
        this.position = Position;
        this.office = Office;
        this.age = Age;
        this.startDate = StartDate;
    }
    getUser() {
        return {
            "ID": this.Id,
            "Name": this.name,
            "Position": this.position,
            "Office": this.office,
            "Age": this.age,
            "StartDate": this.startDate
        }
    }
    getAge() {
        return this.age
    }
    getId() {
        return this.Id
    }
}

var ID_1 = new User(1, "Airi Satou", "Accountant", "Tokyo", "33", "2008/11/28");
var ID_2 = new User(2, "Angelica Ramos", "Chief Executive Officer (CEO)", "London", "47", "2009/10/09");
var ID_3 = new User(3, "Ashton Cox", "Junior Technical Author", "San Francisco", "66", "2009/01/12");
var ID_4 = new User(4, "Bradley Greer", "Software Engineer", "London", "41", "2012/10/13");
var ID_5 = new User(5, "Brenden Wagner", "Software Engineer", "San Francisco", "28", "2011/06/07");
var ID_6 = new User(6, "Brielle Williamson", "AIntegration Specialist", "New York", "61", "2012/12/02");
var ID_7 = new User(7, "Bruno Nash", "Software Engineer", "London", "38", "2011/05/03");
var ID_8 = new User(8, "Caesar Vance", "Pre-Sales Support", "New York", "21", "2011/12/12");
var ID_9 = new User(9, "Cara Stevens", "Sales Assistant", "New York", "46", "2011/12/06");
var ID_10 = new User(10, "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", "22", "2012/03/29");

var allUser = [ID_1, ID_2, ID_3, ID_4, ID_5, ID_6, ID_7, ID_8, ID_9, ID_10];

// Export User
module.exports = { allUser };
